public class KeyboardController{
  
  float x, y;
  float vx,vy;
  int idx;
  float ammount = 5;
  float easing = 0.1;;
  public KeyboardController() {
  }
  void setup() {
  }
  void update() {
    x += vx;
    y += vy;
    vx -= vx/7;
    vy -= vy/7;
    moved(x, y);
  }
  
}
void keyPressed() {
  if (key == CODED) {
    if (keyCode == UP) {
      kc.vy -= kc.ammount;
    } else if (keyCode == DOWN) {
      kc.vy += kc.ammount;
    } else if (keyCode == RIGHT) {
      kc.vx += kc.ammount;
    } else if (keyCode == LEFT) {
      kc.vx -= kc.ammount;
    }  
  } else {
    pressed(kc.x,kc.y);
  }
}