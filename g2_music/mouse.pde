public class MouseController {

  float x, y;
  int idx;
  float easing = 0.1;
  public MouseController() {
  }
  void setup() {
  }
  void update() {
    float targetX = mouseX;
    float dx = targetX - x;
    x += dx * easing;

    float targetY = mouseY;
    float dy = targetY - y;
    y += dy * easing;
    moved(x, y);
  }
}
void mouseClicked() {
  pressed(mouseX, mouseY);
}