

import processing.sound.*;
SoundFile file;

void moved(float x, float y) {
  //println("has moved " + x + " " + y);
  f.x = x; f.y=y;
}
void pressed(float x, float y) {
  println("Has pressed " + x + " " + y);
  f.drop();
}

class Spark {
  PImage sprite;
  float x,y;
  float vy;
  boolean active;
  public Spark(float x,float y) {
    this.x = x; this.y = y;
    sprite = loadImage("brilho1.png");
    sprite.resize(20,20);
    vy =7;
    active = true;
  }
  public void update() {
   y += vy;
   p.sparkSound(this);
   if(y>height) {
     active = false;
   }
   
  }
  public void draw() {
    image(sprite, x-sprite.width/2, y+sprite.height);
  }
  
}

class Fairy {
  PImage[] sprites;
  float x,y;
  int img_index;
  ArrayList<Spark> sparks;
  private int a;
  public Fairy(float x, float y) {
    this.x= x;
    this.y=y;
  }
  
  public void setup() {
    img_index = 0;
    a = 0;
    sprites = new PImage[3];
    sprites[0] = loadImage("fada1.png");
    sprites[1] = loadImage("fada2.png");
    sprites[2] = loadImage("fada3.png");
    
    for(int i =0;i<3;i++) {
      sprites[i].resize(120,60);
    }
    
    sparks = new ArrayList<Spark>();
    println("f setup");
  }
  public void drop() {
    Spark s = new Spark(x+sprites[0].width/2,y);
    sparks.add(s);
  }
  public void update() {
    for(Spark s : sparks) {
      s.update();
    }
  }
  public void draw() {
 
    if(img_index >2){
      img_index=0;
    }
    image(sprites[img_index], x, y);
    a++;
    if(a%7==0)
      img_index++;
    for(Spark s : sparks) {
      s.draw();
    }
  }
}
class Piano {
  PImage referencePiano;
  PImage[] keySprites;
  PImage[] keysUniqueNotPressed;
  PImage[] keysUniquePressed;
  
  SoundFile[] keySounds;
  g2_music ref;
  
  public Piano(g2_music ref) {
     this.ref = ref; 
  }
  
  public void sparkSound(Spark s){
    if(!s.active) {return;}
    if(s.y > 2*height/3.0 && s.y < height) {
      println("Hit");
      int ind = int(s.x/whiteKeyWidth);
      println(ind);
      keySounds[ind].play();
      s.active = false;
    }
    else {
      return;
    }
  }
  public void stopSound(Spark s){
      int ind = int(s.x/whiteKeyWidth);
      //keySounds[ind].stop();
  }
  
  
  float whiteKeyWidth = width/19.0;
  float blackKeyWidth = whiteKeyWidth/2.0;
  
  void setup() {
    keysUniqueNotPressed = new PImage[5];
    keysUniquePressed = new PImage[5];
    
    keysUniqueNotPressed[0] = loadImage("sprites/shape 55 (Up Over).png");
    keysUniqueNotPressed[1] = loadImage("sprites/shape 59 (Up Over).png");
    keysUniqueNotPressed[2] = loadImage("sprites/shape 63 (Up Over).png");
    keysUniqueNotPressed[3] = loadImage("sprites/shape 73 (Up Over).png");
    keysUniqueNotPressed[4] = loadImage("sprites/shape 125 (Up Over).png");
    
    keysUniquePressed[0] = loadImage("sprites/shape 56 (Down Hit).png");
    keysUniquePressed[1] = loadImage("sprites/shape 60 (Down Hit).png");
    keysUniquePressed[2] = loadImage("sprites/shape 64 (Down Hit).png");
    keysUniquePressed[3] = loadImage("sprites/shape 74 (Down Hit).png");
    keysUniquePressed[4] = loadImage("sprites/shape 126 (Down Hit).png");
    referencePiano = loadImage("sprites/mcTeclado.png");
    keySprites = new PImage[10];
    for(int i=0; i< keysUniquePressed.length; i++) {
      image(keysUniquePressed[i],0,0);
    }
    keySounds = new SoundFile[30];
    keySounds[0] = new SoundFile(ref, "keySounds/A3.wav");
    keySounds[1] = new SoundFile(ref, "keySounds/A#3.wav");
    keySounds[2] = new SoundFile(ref, "keySounds/B3.wav");
    keySounds[3] = new SoundFile(ref, "keySounds/C4.wav");
    keySounds[4] = new SoundFile(ref, "keySounds/C#4.wav");
    keySounds[5] = new SoundFile(ref, "keySounds/D4.wav");
    keySounds[6] = new SoundFile(ref, "keySounds/D#4.wav");
    keySounds[7] = new SoundFile(ref, "keySounds/E4.wav");
    keySounds[8] = new SoundFile(ref, "keySounds/F4.wav");
    keySounds[9] = new SoundFile(ref, "keySounds/F#4.wav");
    keySounds[10] = new SoundFile(ref, "keySounds/G4.wav");
    keySounds[11] = new SoundFile(ref, "keySounds/G#4.wav");
    keySounds[12] = new SoundFile(ref, "keySounds/A4.wav");
    keySounds[13] = new SoundFile(ref, "keySounds/A#4.wav");
    keySounds[14] = new SoundFile(ref, "keySounds/B4.wav");
    keySounds[15] = new SoundFile(ref, "keySounds/C5.wav");
    keySounds[16] = new SoundFile(ref, "keySounds/C#5.wav");
    keySounds[17] = new SoundFile(ref, "keySounds/D5.wav");
    keySounds[18] = new SoundFile(ref, "keySounds/D#5.wav");
    keySounds[19] = new SoundFile(ref, "keySounds/E5.wav");
    keySounds[20] = new SoundFile(ref, "keySounds/F5.wav");
    keySounds[21] = new SoundFile(ref, "keySounds/F#5.wav");
    keySounds[22] = new SoundFile(ref, "keySounds/G5.wav");
    keySounds[23] = new SoundFile(ref, "keySounds/G#5.wav");
  }
  void draw() {
    //for(PImage img : keySprites) {
    //  image(img, 0, 0);
    //}
    referencePiano.resize(width,referencePiano.height * width/referencePiano.width);
    image(referencePiano,0,height - referencePiano.height); 
  }
}

class MessageBoard {
  PImage[] messages;
    boolean active;
    int index = 0;
    public MessageBoard() {
      active = false;
      
    }
    void setup() {
      active = false;
       messages = new PImage[4];
       messages[0] = loadImage("tela1.png");
       messages[1] = loadImage("tela2.png");
       messages[2] = loadImage("tela3.png");
       messages[3] = loadImage("tela4.png");
       index = 1;
       for(int i=0;i<messages.length;i++){
         messages[i].resize(300,100);
       }
    }
    void draw() {
      if(active){
          image(messages[index],300,100);
      }
    }
    public void setMessage(int i){
      index = i;
      active = true;
    }
    public void stopMessage() {
       active = false;
    }
}

Fairy f;
MouseController mc;
Piano p;
KeyboardController kc;
MessageBoard m;

void setup() {
  size(1024, 768);
  frameRate(70);
  p = new Piano(this);
  f = new Fairy(0,0);
  f.setup();
  kc = new KeyboardController();
  kc.setup();
  m = new MessageBoard();
  m.setup();
  m.setMessage(3);
  //mc = new MouseController();
  //mc.setup();
  p.setup();
  //doSetup();
}

void draw() {
  background(255);
  kc.update();
  //mc.update();
  f.update();
  f.draw();
  p.draw();
  m.draw();
  //doDraw();

}